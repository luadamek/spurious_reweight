import os
import numpy as np
import pickle
import matplotlib.pyplot as plt
import xgboost
import ROOT as r
import utils
import datasets

def create_histograms(njet, bins, hrange, variable):

    print(njet, variable, bins, hrange)

    def create_histogram(var, weight, error, bins, hrange, hname):
        values, _ = np.histogram(var, bins=bins, range=hrange, weights=weight)
        # poisson uncertainty (sum of square of weights)
        poiss_unc_sq, _ = np.histogram(var, bins=bins, range=hrange, weights=weight**2)
        poiss_unc = poiss_unc_sq**0.5
        # uncertainty on the re-weighting
        if not type(error) == type(None):
            err_sq, _ = np.histogram(var, bins=bins, range=hrange, weights=(weight*error)**2)
            errs = err_sq**0.5

        h = r.TH1D(hname, 'title', bins, hrange[0], hrange[1])
        for b in range(bins):
            # set the value
            h.SetBinContent(b+1, values[b])
            # set the error
            if not type(error) == type(None):
                total_error = (poiss_unc[b]**2 + errs[b]**2)**0.5
            else:
                total_error = poiss_unc[b]
            h.SetBinError(b, total_error)
        return h

    # loop over all data
    data_files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_data_minituple_files(njet)]
    
    for fpath in data_files:
        print(fpath)
        for fold in [0, 1, 2]:
            _, __, z_test = datasets.get_Z(fpath, njet, fold)
            var = z_test[variable]
            weights = z_test['Weight']

            hname = 'h_{}_{}_fold{}.root'.format(variable, os.path.basename(fpath)[:-5], fold)
            h = create_histogram(var, weights, None, bins, hrange, hname)

            # write to file
            fname = 'h_{}_{}_fold{}.root'.format(variable, os.path.basename(fpath)[:-5], fold)
            rf = r.TFile('../histograms/{}'.format(fname), 'recreate')
            h.Write()
            rf.Close()

    # loop over all the files
    files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_ss_minituple_files(njet)]

    for fpath in files:

        print(fpath)

        for fold in [0,1,2]:
            
            print(fold)

            # load the dataframe in each fold
            _, __, x_test = datasets.get_X(fpath, njet, fold, training=False)
            _, __, z_test = datasets.get_Z(fpath, njet, fold)

            # load the xgboost and transformer
            model = xgboost.Booster({'nthread': 4})
            model.load_model('../models/xgb_{}jet_fold{}.xgb'.format(njet, fold))

            with open('../models/tsf_{}jet_fold{}.pkl'.format(njet, fold), 'rb') as handle:
                tsf = pickle.load(handle)

            # predict and transform the score
            dtest = xgboost.DMatrix(x_test)
            y_pred = model.predict(dtest)
            y_tsf = tsf.transform(y_pred.reshape(-1,1))

            # load the weight ratio histogram 
            edges, contents, errors = utils.load_weight_histo(njet)
            idx = np.searchsorted(edges, y_tsf.ravel()) - 1
            mva_weights = contents[idx]
            mva_errors = errors[idx]

            # compute the inclusive mass shape
            var = z_test[variable]
            weights = z_test['Weight']

            # collection of hists to write to file
            hists = []

            # create the histogram
            hname = 'h_{}_{}_fold{}.root'.format(variable, os.path.basename(fpath)[:-5], fold)
            h = create_histogram(var, weights, None, bins, hrange, hname)
            w_h = create_histogram(var, (weights)*(mva_weights), mva_errors, bins, hrange, 'w_'+hname)
            hists.extend([h, w_h])

            # write to file
            fname = 'h_{}_{}_fold{}.root'.format(variable, os.path.basename(fpath)[:-5], fold)
            print(fname)
            rf = r.TFile('../histograms/{}'.format(fname), 'recreate')
            for h in hists:
                h.Write()
            rf.Close()
        break

def compute_ratio(njet, bins, hrange, variable):
    """
    Ratio between MC and data. (re-weighted and not re-weighted)
    """

    # load all the histograms for ss
    def get_hs(hname):
        rf = r.TFile('../histograms/{}'.format(hname), 'read')
        hs = []
        for k in rf.GetListOfKeys():
            hs.append(rf.Get(k.GetName()))
            hs[-1].SetDirectory(0)
        rf.Close
        return hs

    # load all the histograms
    hnames = [n for n in os.listdir('../histograms/') if str(njet)+'jet' in n and variable in n]
    histograms = [get_hs(hname) for hname in hnames if 'ntuple' in hname or 'data' in hname]

    # add them all together
    total = r.TH1D('total_{}_{}jet'.format(variable, njet), 'title', bins, hrange[0], hrange[1])
    w_total = r.TH1D('w_total_{}_{}jet'.format(variable, njet), 'title', bins, hrange[0], hrange[1])
    data = r.TH1D('data_total_{}_{}jet'.format(variable, njet), 'title', bins, hrange[0], hrange[1])
    total.Sumw2()
    w_total.Sumw2()
    data.Sumw2()

    for hlist in histograms:
        for h in hlist:
            hname = h.GetName()
            if hname.startswith('h_') and 'ntuple' in hname:
                total.Add(h)
            elif hname.startswith('w_h') and 'ntuple' in hname:
                w_total.Add(h)
            elif hname.startswith('h_') and 'data' in hname:
                data.Add(h)

    # take the ratio
    ratio = data.Clone('ratio')
    ratio.Divide(total)
    w_ratio = data.Clone('w_ratio')
    w_ratio.Divide(w_total)

    # save them all
    rf = r.TFile('../histograms/total_{}_{}jet.root'.format(variable, njet), 'recreate')
    total.Write()
    w_total.Write()
    data.Write()
    ratio.Write()
    w_ratio.Write()
    rf.Close()

def plot_ratios(njet, variable):

    rf = r.TFile('../histograms/total_{}_{}jet.root'.format(variable, njet), 'read')
    mc = rf.Get('total_{}_{}jet'.format(variable, njet))
    w_mc = rf.Get('w_total_{}_{}jet'.format(variable, njet))
    data = rf.Get('data_total_{}_{}jet'.format(variable, njet))
    ratio = rf.Get('ratio')
    w_ratio = rf.Get('w_ratio')

    # load histogram
    def load_histo(h):
        bins = h.GetNbinsX()
        edges = np.linspace(h.GetBinLowEdge(1), h.GetBinLowEdge(bins+1), bins+1)
        centres = (edges[1:] + edges[:-1]) / 2.
        contents = np.zeros(bins)
        errors = np.zeros(bins)
        for b in range(bins):
            contents[b] = h.GetBinContent(b)
            errors[b] = h.GetBinError(b)

        return centres, contents, errors

    # make the plot
    dark_blue  = (4*1./255, 30*1./255, 66*1./255) # pantone 282
    blue = (72*1./255, 145*1./255, 220*1./255) # pantone279 
    light_blue = (158*1./255, 206*1./255, 235*1./255) # pantone291
    fig, ax = plt.subplots(2, gridspec_kw={'height_ratios':[3, 1]})

    # data
    centres, contents, errors = load_histo(data)
    sum_data = np.sum(contents)
    ax[0].errorbar(centres, contents, yerr=errors, color='k', label='data')

    # mc
    centres, contents, errors = load_histo(mc)
    norm = sum_data / np.sum(contents)
    ax[0].errorbar(centres, norm*contents, yerr=norm*errors, color=light_blue, label='unweighted SS')

    # mc
    centres, contents, errors = load_histo(w_mc)
    w_norm = sum_data / np.sum(contents)
    ax[0].errorbar(centres, w_norm*contents, yerr=w_norm*errors, color=blue, label='re-weighted SS')

    # mc ratio
    centres, contents, errors = load_histo(ratio)
    ax[1].errorbar(centres, contents/norm, yerr=errors/norm, color=light_blue)

    # r.w. mc ratio
    centres, contents, errors = load_histo(w_ratio)
    ax[1].errorbar(centres, contents/w_norm, yerr=errors/w_norm, color=blue)

    # cosmetics
    ax[0].legend(loc='best')
    ax[1].set_xlabel('{} ({} jet channel)'.format(variable, njet))
    ax[1].set_ylim(0, 3)
    name = '../histograms/input_plot_{}_{}jet.pdf'.format(variable, njet)
    plt.savefig(name)
    print(name)
    

def main():

    binning = 0.1
    hrange = (100, 300)
    bins = int((hrange[1] - hrange[0]) / binning)
    variable = 'M_MuMu_FSR'

    setups = [
              ['DimuonPT', 1, (0, 150)],
              ['AbsCosThetaStar', 0.05, (0, 1)],
              ['DimuonY', 0.1, (-2.5, 2.5)],
              ['LeadJetPt', 1, (0, 200)],
              ['LeadJetEta', 0.1, (-4, 4)],
              ['LeadJetDPhiZ', 0.1, (-3.2, 3.2)],
              ['SubleadJetPt', 1, (0, 150)],
              ['SubleadJetEta', 0.1, (-4, 4)],
              ['SubleadJetDPhiZ', 0.1, (-3.2, 3.2)],
              ['DijetPt', 1, (0, 500)],
              ['DijetRapidity', 0.1, (-4, 4)],
              ['DijetDPhiZ', 0.1, (-3.2, 3.2)],
              ['DijetM', 5, (0, 1000)],
             ]

    # loop over all njets
    #for njet in [0, 1, 2]:
    for njet in [2]:

        print()
        print('==============')
        print('{} jets'.format(njet))
        print('==============')
        print()

        # loop over all variables
        for setup in setups:
            variable, binning, hrange = setup
            bins = int((hrange[1] - hrange[0]) / binning)

            if njet == 0 and variable not in ['DimuonPT', 'AbsCosThetaStar', 'DimuonY']:
                break
            if njet == 1 and variable not in ['DimuonPT', 'AbsCosThetaStar', 'DimuonY', 'LeadJetPt', 'LeadJetEta', 'LeadJetDPhiZ']:
                break

            #create_histograms(njet, bins, hrange, variable)
            compute_ratio(njet, bins, hrange, variable)
            plot_ratios(njet, variable)


if __name__ == '__main__':
    main()
