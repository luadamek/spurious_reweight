import os
import numpy as np
import pickle
import matplotlib.pyplot as plt
import xgboost
import ROOT as r
import utils
import datasets

def create_histograms(njet, bins, hrange):

    # loop over all the files
    files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_ss_minituple_files(njet)]

    for fpath in files:

        print(fpath)

        for fold in [0,1,2]:

            # load the dataframe in each fold
            _, __, x_test = datasets.get_X(fpath, njet, fold, training=False)
            _, __, z_test = datasets.get_Z(fpath, njet, fold)

            # load the xgboost and transformer
            model = xgboost.Booster({'nthread': 4})
            model.load_model('../models/xgb_{}jet_fold{}.xgb'.format(njet, fold))

            with open('../models/tsf_{}jet_fold{}.pkl'.format(njet, fold), 'rb') as handle:
                tsf = pickle.load(handle)

            # predict and transform the score
            dtest = xgboost.DMatrix(x_test)
            y_pred = model.predict(dtest)
            y_tsf = tsf.transform(y_pred.reshape(-1,1))

            # load the weight ratio histogram 
            edges, contents, errors = utils.load_weight_histo(njet)
            idx = np.searchsorted(edges, y_tsf.ravel()) - 1
            mva_weights = contents[idx]
            mva_errors = errors[idx]

            # compute the inclusive mass shape
            mass = z_test['M_MuMu_FSR']
            weights = z_test['Weight']
            xgb_higgs = z_test['ClassOut_XGB_fJVT_Higgs']
            xgb_vbf = z_test['ClassOut_XGB_fJVT_VBF']

            def create_histogram(mass, weight, error, bins, hrange, hname):
                values, _ = np.histogram(mass, bins=bins, range=hrange, weights=weight)
                # poisson uncertainty (sum of square of weights)
                poiss_unc_sq, _ = np.histogram(mass, bins=bins, range=hrange, weights=weight**2)
                poiss_unc = poiss_unc_sq**0.5
                # uncertainty on the re-weighting
                if not type(error) == type(None):
                    err_sq, _ = np.histogram(mass, bins=bins, range=hrange, weights=(weight*error)**2)
                    errs = err_sq**0.5

                h = r.TH1D(hname, 'title', bins, hrange[0], hrange[1])
                for b in range(bins):
                    # set the value
                    h.SetBinContent(b+1, values[b])
                    # set the error
                    if not type(error) == type(None):
                        total_error = (poiss_unc[b]**2 + errs[b]**2)**0.5
                    else:
                        total_error = poiss_unc[b]
                    h.SetBinError(b, total_error)
                return h

            # collection of hists to write to file
            hists = []

            if njet in [0, 1]:
                categories = ['inclusive', 1, 2, 3]
                for cat in categories:
                    print(cat)
                    # cuts
                    lo, hi = utils.xgb_higgs_boundaries(njet, cat)
                    mask1 = xgb_higgs > lo
                    mask2 = xgb_higgs < hi
                    mask = np.logical_and(mask1, mask2)
                    
                    # create the histogram
                    hname = 'histogram_mass_{}_{}_fold{}.root'.format(os.path.basename(fpath)[:-5], cat, fold)
                    h = create_histogram(mass[mask], weights[mask], None, bins, hrange, hname)
                    w_h = create_histogram(mass[mask], (weights[mask])*(mva_weights[mask]), mva_errors[mask], bins, hrange, 'w_'+hname)
                    hists.extend([h, w_h])

            if njet == 2:
                categories = ['inclusive', 1, 2, 3, 4, 5, 6]
                for cat in categories:
                    print(cat)

                    # cuts
                    if cat in ['inclusive', 1, 2, 3]:
                        lo, hi = utils.xgb_vbf_boundaries(njet, cat)
                        mask1 = xgb_higgs > lo
                        mask2 = xgb_higgs < hi
                        mask = np.logical_and(mask1, mask2)
                    elif cat in [4, 5, 6]:
                        lo, hi = utils.xgb_higgs_boundaries(njet, cat-3)
                        mask1 = xgb_higgs > lo
                        mask2 = xgb_higgs < hi
                        mask_higgs = np.logical_and(mask1, mask2)
                        mask_vbf = xgb_vbf < 0.60
                        mask = np.logical_and(mask_higgs, mask_vbf)

                    # create the histogram
                    hname = 'histogram_mass_{}_{}_fold{}.root'.format(os.path.basename(fpath)[:-5], cat, fold)
                    h = create_histogram(mass[mask], weights[mask], None, bins, hrange, hname)
                    w_h = create_histogram(mass[mask], (weights[mask])*(mva_weights[mask]), mva_errors[mask], bins, hrange, 'w_'+hname)
                    hists.extend([h, w_h])

            # write to file
            fname = 'histogram_mass_{}_fold{}.root'.format(os.path.basename(fpath)[:-5], fold)
            print(fname)
            rf = r.TFile('../histograms/{}'.format(fname), 'recreate')
            for h in hists:
                h.Write()
            rf.Close()

def compute_ratio(njet, bins, hrange):

    # load all the histograms for ss
    def get_hs(hname):
        rf = r.TFile('../histograms/{}'.format(hname), 'read')
        hs = []
        for k in rf.GetListOfKeys():
            hs.append(rf.Get(k.GetName()))
            hs[-1].SetDirectory(0)
        rf.Close
        return hs

    # load all the histograms
    hnames = [n for n in os.listdir('../histograms/') if str(njet)+'jet' in n and 'mass' in n]
    histograms = [get_hs(hname) for hname in hnames if 'ntuple' in hname]

    # determine the categories
    if njet in [0, 1]:
        categories = ['inclusive', 1, 2, 3]
    if njet == 2:
        categories = ['inclusive', 1, 2, 3, 4, 5, 6]

    # loop over the categories
    totals = []
    w_totals = []
    ratios = []
    for cat in categories:

        # add them all together
        total = r.TH1D('total_mass_{}jet_{}'.format(njet, cat), 'title', bins, hrange[0], hrange[1])
        w_total = r.TH1D('w_total_mass_{}jet_{}'.format(njet, cat), 'title', bins, hrange[0], hrange[1])
        total.Sumw2()
        w_total.Sumw2()

        hs = []
        w_hs = []
        for hlist in histograms:
            for h in hlist:
                hname = h.GetName()
                if '_{}_'.format(cat) in hname and hname.startswith('histogram'):
                    hs.append(h)
                elif '_{}_'.format(cat) in hname and hname.startswith('w_histogram'):
                    w_hs.append(h)

        for h in hs:
            total.Add(h)
        for h in w_hs:
            w_total.Add(h)

        # take the ratio
        ratio = w_total.Clone('ratio_{}'.format(cat))
        ratio.Divide(total)

        # save them
        totals.append(total)
        w_totals.append(w_total)
        ratios.append(ratio)

    # save them all
    rf = r.TFile('../histograms/mass_shape_{}jet.root'.format(njet), 'recreate')
    for h in totals+w_totals+ratios:
        h.Write()
    rf.Close()

def plot_ratios(njet):

    rf = r.TFile('../histograms/mass_shape_{}jet.root'.format(njet), 'read')
    hs = []
    for k in rf.GetListOfKeys():
        if 'ratio' in k.GetName():
            hs.append(rf.Get(k.GetName()))
            hs[-1].SetDirectory(0)
            hs[-1].Rebin(10)
    rf.Close()

    # loop over all histograms
    for h in hs:
        
        # load histogram
        bins = h.GetNbinsX()
        edges = np.linspace(h.GetBinLowEdge(1), h.GetBinLowEdge(bins+1), bins+1)
        centres = (edges[1:] + edges[:-1]) / 2.
        contents = np.zeros(bins)
        errors = np.zeros(bins)
        for b in range(bins):
            contents[b] = h.GetBinContent(b)
            errors[b] = h.GetBinError(b)

        # make the plot
        dark_blue  = (4*1./255, 30*1./255, 66*1./255) # pantone 282
        cat = h.GetName()[6:]
        fig, ax = plt.subplots()
        binning = (edges[-1] - edges[0]) / bins
        normalisation = 1 /np.mean(contents[int(1./binning)*10:int(1./binning)*60]) # 100-300 is the full range, show between 110 and 160
        ys = contents * normalisation
        yerr = errors * normalisation
        ax.errorbar(centres, ys, yerr=yerr, color=dark_blue) 
        ax.set_title('Weighted/unweighted fastMC ({} jet, {})'.format(njet, cat))
        ax.set_xlim(110, 160)
        if njet == 2:
            #ax.set_ylim(0, 2)
            ax.set_ylim(0.8, 1.2)
        if njet == 1:
            #ax.set_ylim(0.90, 1.1)
            ax.set_ylim(0.95, 1.05)
        elif njet == 0:
            #ax.set_ylim(0.95, 1.05)
            ax.set_ylim(0.98, 1.02)
        ax.set_xlabel('Invariant Mass (GeV)')
        name = '../histograms/mass_plot_{}jet_{}.pdf'.format(njet, cat)
        plt.savefig(name)
        print(name)
    

def main():

    binning = 0.1
    hrange = (100, 300)
    bins = int((hrange[1] - hrange[0]) / binning)

    # loop over all njets, files
    #for njet in [0, 1, 2]:
    for njet in [2]:

        create_histograms(njet, bins, hrange)
        compute_ratio(njet, bins, hrange)
        plot_ratios(njet)


if __name__ == '__main__':
    main()
