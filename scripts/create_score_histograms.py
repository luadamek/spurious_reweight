import os
import numpy as np
import pickle
import xgboost
import ROOT as r
import utils
import datasets

def create_histograms(njet, bins, hrange):

    # data files
    data_files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_data_minituple_files(njet)]

    # ss files
    ss_files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_ss_minituple_files(njet)]

    # loop over all the files
    for fpath in data_files + ss_files:

        print(fpath)

        for fold in [0,1,2]:

            # load the dataframe in each fold
            _, __, x_test = datasets.get_X(fpath, njet, fold, training=False)
            _, __, z_test = datasets.get_Z(fpath, njet, fold)

            # load the xgboost and transformer
            model = xgboost.Booster({'nthread': 4})
            model.load_model('../models/xgb_{}jet_fold{}.xgb'.format(njet, fold))

            with open('../models/tsf_{}jet_fold{}.pkl'.format(njet, fold), 'rb') as handle:
                tsf = pickle.load(handle)

            # predict and transform the score
            dtest = xgboost.DMatrix(x_test)
            y_pred = model.predict(dtest)
            y_tsf = tsf.transform(y_pred.reshape(-1,1))

            # histogram the values
            values, edges = np.histogram(y_tsf.ravel(), bins=bins, range=hrange, weights=z_test['Weight'])

            # create a root histogram and save it
            fname = 'histogram_score_{}_{}jet_fold{}.root'.format(os.path.basename(fpath)[:-5], njet, fold)
            h = r.TH1D(fname, 'title', bins, hrange[0], hrange[1])
            for b in range(bins):
                h.SetBinContent(b+1, values[b])

            # write to file
            rf = r.TFile('../histograms/{}'.format(fname), 'recreate')
            h.Write()
            rf.Close()
    
def compute_ratio(njet, bins, hrange):

    # load all the histograms for ss
    def get_h(hname):
        rf = r.TFile('../histograms/{}'.format(hname), 'read')
        h = rf.Get(hname)
        h.SetDirectory(0)
        rf.Close
        return h

    hnames = [n for n in os.listdir('../histograms/') if str(njet)+'jet' in n and 'score' in n]
    data_hs = [get_h(hname) for hname in hnames if 'allYear' in hname]
    ss_hs = [get_h(hname) for hname in hnames if 'ntuple' in hname]
    
    # add them all together
    total_data = r.TH1D('total_data_{}jet'.format(njet), 'title', bins, hrange[0], hrange[1])
    total_ss = r.TH1D('total_ss_{}jet'.format(njet), 'title', bins, hrange[0], hrange[1])

    for h in data_hs:
        total_data.Add(h)
    for h in ss_hs:
        total_ss.Add(h)

    # define the errors
    total_data.Sumw2()
    total_ss.Sumw2()

    # compute the ratio
    ratio = total_data.Clone('ratio_{}jet'.format(njet))
    ratio.Divide(total_ss)

    # save them all
    for h in [total_data, total_ss, ratio]:
        rf = r.TFile('../histograms/{}.root'.format(h.GetName()), 'recreate')
        h.Write()
        rf.Close()


def main():

    # define the xgb score histogram
    bins = 100
    hrange = (0,1)

    # loop over all njets
    for njet in [0, 1, 2]:

        create_histograms(njet, bins, hrange)
        compute_ratio(njet, bins, hrange)


if __name__ == '__main__':
    main()
