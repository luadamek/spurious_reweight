import os
import numpy as np
import argparse
import pickle
import itertools
import matplotlib.pyplot as plt
import xgboost
import sklearn
from sklearn.preprocessing import QuantileTransformer
import ROOT as r
import utils
import datasets

def combine(files, njet, fold, max_events=np.inf):
    """
    Combine the files in njet channel and fold.
    """

    # keep track of the number of events in case the maximum number of events is required.
    N_events = 0

    # collect
    trains, valids, tests = [], [], []
    for f in files:
        print(f)
        train, valid, test = datasets.get_X(f, njet, fold, training=True)
        trains.append(train)
        valids.append(valid)
        tests.append(test)

        N_events += len(train)
        if N_events > max_events:
            break

    # and combine
    return np.concatenate(trains), np.concatenate(valids), np.concatenate(tests)


def get_training_arrays(data_files, ss_files, njet, fold):
    """
    Get training-ready numpy arrays, combining data and ss events.
    """
    
    # get the x arrays
    data_x_train, data_x_valid, data_x_test = combine(data_files, njet, fold)
    ss_x_train, ss_x_valid, ss_x_test = combine(ss_files, njet, fold, max_events=len(data_x_train))

    # create the y arrays
    data_y_train, data_y_valid, data_y_test = np.ones(len(data_x_train)), np.ones(len(data_x_valid)), np.ones(len(data_x_test))
    ss_y_train, ss_y_valid, ss_y_test = np.zeros(len(ss_x_train)), np.zeros(len(ss_x_valid)), np.zeros(len(ss_x_test))

    # combine
    x_train, y_train = np.concatenate([data_x_train, ss_x_train]), np.concatenate([data_y_train, ss_y_train])
    x_valid, y_valid = np.concatenate([data_x_valid, ss_x_valid]), np.concatenate([data_y_valid, ss_y_valid])
    x_test, y_test = np.concatenate([data_x_test, ss_x_test]), np.concatenate([data_y_test, ss_y_test])

    return x_train, y_train, x_valid, y_valid, x_test, y_test


def main():

    parser = argparse.ArgumentParser(description='QuickTrain')
    parser.add_argument('--njet', '-n',
                        default=[0],
                        type=int,
                        nargs='+',
                        choices=[0,1,2],
                        help='number of jets channel')
    parser.add_argument('--fold', '-f',
                        default=[0],
                        type=int,
                        nargs='+',
                        choices=[0,1,2],
                        help='Which fold (N/3) to use. N means EventNumber % 3 == N is the test set.')
    args = parser.parse_args()

    for njet, fold in itertools.product(args.njet, args.fold):
    
        # get files
        data_files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_data_minituple_files(njet)]
        ss_files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_ss_minituple_files(njet)]
    
        # prepare the features and targets
        x_train, y_train, x_valid, y_valid, x_test, y_test = get_training_arrays(data_files, ss_files, njet, fold)
    
        # prepare the xgb matrices
        dtrain = xgboost.DMatrix(x_train, label=y_train)
        dvalid = xgboost.DMatrix(x_valid, label=y_valid)
        dtest = xgboost.DMatrix(x_test, label=y_test)
    
        # prepare the xgb parameters
        param = {'max_depth': 10,
                 'eta': 0.1,
                 'silent': 1,
                 'colsample_bytree': 0.85,
                 'max_delta_step': 10,
                 'min_child_weight': 80,
                 'max_bin': 400,
                 'alpha': 0.9,
                 'gamma': 10
                 }
        num_round = 1000
    
        # train the model 
        model = xgboost.train(param, dtrain, num_round, evals=[(dvalid, 'valid')], early_stopping_rounds=10)
    
        # test the model
        y_pred = model.predict(dtest)
        auroc = sklearn.metrics.roc_auc_score(y_test, y_pred)
        print('AUROC:', auroc)
    
        # derive the transformation (be flaat in data, which has lower stats, so that r.w. has low errors)
        tsf = QuantileTransformer(output_distribution='uniform')
        y_pred_data = y_pred[y_test == 1]
        tsf.fit(y_pred_data.reshape(-1,1))
    
        # save the model and the transformer
        model.save_model('../models/xgb_{}jet_fold{}.xgb'.format(njet, fold))
        with open('../models/tsf_{}jet_fold{}.pkl'.format(njet, fold), 'wb') as handle:
            pickle.dump(tsf, handle)

        # colours
        dark_blue  = (4*1./255, 30*1./255, 66*1./255) # pantone 282
        blue = (72*1./255, 145*1./255, 220*1./255) # pantone279 
        light_blue = (158*1./255, 206*1./255, 235*1./255) # pantone291

        # plot the performance as a ROC curve
        fig, ax = plt.subplots()

        # plot train, valid, test sets
        fpr, tpr, _ = sklearn.metrics.roc_curve(y_test, model.predict(dtest))
        ax.plot(fpr, tpr, color=dark_blue, label='test')
        fpr, tpr, _ = sklearn.metrics.roc_curve(y_valid, model.predict(dvalid))
        ax.plot(fpr, tpr, color=blue, label='valid')
        fpr, tpr, _ = sklearn.metrics.roc_curve(y_train, model.predict(dtrain))
        ax.plot(fpr, tpr, color=light_blue, label='train')

        ax.set_xlabel('False positive rate')
        ax.set_ylabel('True positive rate')
        ax.set_title('{} jet, fold {}. Test AUROC: {}'.format(njet, fold, auroc))
        ax.legend(loc='best')
        plt.savefig('../models/perf_{}jet_fold{}.pdf'.format(njet, fold))

    
    
if __name__ == '__main__':
    main()


