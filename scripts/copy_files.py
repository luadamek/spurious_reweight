import argparse
from os import system
from file_list import file_dict

parser = argparse.ArgumentParser(description='download files')
parser.add_argument('--ss_flavour', '-ssflav', dest="ss_flavour", type=str, required=True, help='the type of ss nutple to copy. can be mumu95_incl or mumu95_jj')
parser.add_argument('--test', '-t', dest="test", type=bool, required=True, help="Should we copy all of the files? or a subset")
parser.add_argument('--outDir', '-od', dest='output_dir', type=str, required=True)

args = parser.parse_args()

output_dir = args.output_dir
test = args.test
ss_flavour = args.ss_flavour

files = file_dict[ss_flavour]

if test:
    files = files[:2]

for f in files:
    command = "xrdcp root://eosatlas.cern.ch://eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/ss_ntuples/v12/{a}/{b} {c}".format(a=ss_flavour, b=f, c=output_dir)
    system(command)
    
