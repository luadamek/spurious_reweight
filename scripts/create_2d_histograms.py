import os
import numpy as np
import pickle
import itertools
import matplotlib.pyplot as plt
import xgboost
import ROOT as r
import utils
import datasets

def create_histograms(njet, xsetup, ysetup):

    # get histogram info
    xvar, xbins, xhrange = xsetup
    yvar, ybins, yhrange = ysetup

    # loop over all the files
    files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_ss_minituple_files(njet)]
    data_files = [os.path.join(utils.get_minituple_loc(), name) for name in utils.get_data_minituple_files(njet)]

    for fpath in data_files:

        print(fpath)

        for fold in [0,1,2]:

            # load the dataframe in each fold
            _, __, x_test = datasets.get_X(fpath, njet, fold, training=False)
            _, __, z_test = datasets.get_Z(fpath, njet, fold)

            # load the xgboost and transformer
            model = xgboost.Booster({'nthread': 4})
            model.load_model('../models/xgb_{}jet_fold{}.xgb'.format(njet, fold))

            with open('../models/tsf_{}jet_fold{}.pkl'.format(njet, fold), 'rb') as handle:
                tsf = pickle.load(handle)

            # predict and transform the score
            dtest = xgboost.DMatrix(x_test)
            y_pred = model.predict(dtest)
            y_tsf = tsf.transform(y_pred.reshape(-1,1))

            # compute the inclusive mass shape
            xvals = z_test[xvar]
            yvals = z_test[yvar]
            weights = z_test['Weight']

            def create_2d_histogram(hname, xvals, yvals, weights, xsetup, ysetup):

                # get histogram info
                xvar, xbins, xhrange = xsetup
                xlo, xhi = xhrange[0], xhrange[1]
                yvar, ybins, yhrange = ysetup
                ylo, yhi = yhrange[0], yhrange[1]

                # central values
                values, _, _ = np.histogram2d(xvals, yvals,
                                            bins=[xbins, ybins],
                                            range=[(xlo, xhi), (ylo, yhi)],
                                            weights=weights,
                                            normed=False)
                # errors
                poiss_unc_sq, _, _ = np.histogram2d(xvals, yvals,
                                            bins=[xbins, ybins],
                                            range=[(xlo, xhi), (ylo, yhi)],
                                            weights=weights**2,
                                            normed=False)
                poiss_unc = poiss_unc_sq**0.5

                # turn it into a root TH1D
                h = r.TH2D(hname, 'title', xbins, xlo, xhi, ybins, ylo, yhi)
                for bx, by in itertools.product(range(xbins), range(ybins)):
                    h.SetBinContent(bx+1, by+1, values[bx, by])
                    h.SetBinError(bx+1, by+1, poiss_unc[bx, by])

                return h

            # create the histogram
            hname = 'h2d__{}__{}__{}__{}'.format(os.path.basename(fpath)[:-5], 'fold'+str(fold), xvar, yvar)
            h2d = create_2d_histogram(hname, xvals, yvals, weights, xsetup, ysetup)

            # write to file
            fname = hname+'.root'
            print(fname)
            rf = r.TFile('../histograms/{}'.format(fname), 'recreate')
            h2d.Write('data')
            rf.Close()

    for fpath in files[:5]:

        print(fpath)

        for fold in [0,1,2]:

            # load the dataframe in each fold
            _, __, x_test = datasets.get_X(fpath, njet, fold, training=False)
            _, __, z_test = datasets.get_Z(fpath, njet, fold)

            # load the xgboost and transformer
            model = xgboost.Booster({'nthread': 4})
            model.load_model('../models/xgb_{}jet_fold{}.xgb'.format(njet, fold))

            with open('../models/tsf_{}jet_fold{}.pkl'.format(njet, fold), 'rb') as handle:
                tsf = pickle.load(handle)

            # predict and transform the score
            dtest = xgboost.DMatrix(x_test)
            y_pred = model.predict(dtest)
            y_tsf = tsf.transform(y_pred.reshape(-1,1))

            # load the weight ratio histogram 
            edges, contents, errors = utils.load_weight_histo(njet)
            idx = np.searchsorted(edges, y_tsf.ravel()) - 1
            mva_weights = contents[idx]
            mva_errors = errors[idx]

            # compute the inclusive mass shape
            xvals = z_test[xvar]
            yvals = z_test[yvar]
            weights = z_test['Weight']

            def create_2d_histogram(hname, xvals, yvals, weights, xsetup, ysetup):

                # get histogram info
                xvar, xbins, xhrange = xsetup
                xlo, xhi = xhrange[0], xhrange[1]
                yvar, ybins, yhrange = ysetup
                ylo, yhi = yhrange[0], yhrange[1]

                # central values
                values, _, _ = np.histogram2d(xvals, yvals,
                                            bins=[xbins, ybins],
                                            range=[(xlo, xhi), (ylo, yhi)],
                                            weights=weights,
                                            normed=False)
                # errors
                poiss_unc_sq, _, _ = np.histogram2d(xvals, yvals,
                                            bins=[xbins, ybins],
                                            range=[(xlo, xhi), (ylo, yhi)],
                                            weights=weights**2,
                                            normed=False)
                poiss_unc = poiss_unc_sq**0.5

                # turn it into a root TH1D
                h = r.TH2D(hname, 'title', xbins, xlo, xhi, ybins, ylo, yhi)
                for bx, by in itertools.product(range(xbins), range(ybins)):
                    h.SetBinContent(bx+1, by+1, values[bx, by])
                    h.SetBinError(bx+1, by+1, poiss_unc[bx, by])

                return h

            # create the histogram
            hname_norw = 'h2d__noRW__{}__{}__{}__{}'.format(os.path.basename(fpath)[:-5], 'fold'+str(fold), xvar, yvar)
            h2d_norw = create_2d_histogram(hname_norw, xvals, yvals, weights, xsetup, ysetup)
            # unreweighted
            hname = 'h2d__{}__{}__{}__{}'.format(os.path.basename(fpath)[:-5], 'fold'+str(fold), xvar, yvar)
            h2d = create_2d_histogram(hname, xvals, yvals, weights*mva_weights, xsetup, ysetup)

            # write to file
            fname = hname+'.root'
            print(fname)
            rf = r.TFile('../histograms/{}'.format(fname), 'recreate')
            h2d.Write('reweighted_ss')
            h2d_norw.Write('unreweighted_ss')
            rf.Close()



def sum_histograms(njet, xsetup, ysetup):

    # get histogram info
    xvar, xbins, xhrange = xsetup
    yvar, ybins, yhrange = ysetup
    xlo, xhi = xhrange[0], xhrange[1]
    ylo, yhi = yhrange[0], yhrange[1]

    # load all the histograms for ss
    def get_hs(hname):
        rf = r.TFile('../histograms/{}'.format(hname), 'read')
        hs = []
        for k in rf.GetListOfKeys():
            h = rf.Get(k.GetName())
            h.SetDirectory(0)
            hs.append(h)
        rf.Close
        return hs

    # load all the histograms
    hnames = [n for n in os.listdir('../histograms/') if str(njet)+'jet' in n and '{}__{}'.format(xvar, yvar) in n]

    hs_ss = []
    hs_ss_norw = []
    hs_data = []
    for hname in hnames:
        if 'ntuple' in hname:
            both = get_hs(hname)
            hs_ss.extend([h for h in both if not 'h2d__noRW' in h.GetName()])
            hs_ss_norw.extend([h for h in both if 'h2d__noRW' in h.GetName()])
        elif 'data' in hname:
            hs_data.extend(get_hs(hname))

    # add them all together
    hname = 'total2d_norw__{}jet__{}__{}'.format(njet, xvar, yvar)
    total_ss_norw = r.TH2D(hname, 'title', xbins, xlo, xhi, ybins, ylo, yhi)
    total_ss_norw.Sumw2()

    hname = 'total2d__{}jet__{}__{}'.format(njet, xvar, yvar)
    total_ss = r.TH2D(hname, 'title', xbins, xlo, xhi, ybins, ylo, yhi)
    total_ss.Sumw2()

    total_data = r.TH2D('data__{}jet__{}__{}'.format(njet, xvar, yvar), 'title', xbins, xlo, xhi, ybins, ylo, yhi)
    total_data.Sumw2()

    for h in hs_ss:
        total_ss.Add(h)
    for h in hs_ss_norw:
        total_ss_norw.Add(h)
    for h in hs_data:
        total_data.Add(h)

    # normalise the ss to data
    sum_ss = total_ss.Integral()
    sum_data = total_data.Integral()
    total_ss.Scale(sum_data/total_ss.Integral())
    total_ss_norw.Scale(sum_data/total_ss_norw.Integral())

    # save them all
    fname = hname+'.root'
    print(fname)
    rf = r.TFile('../histograms/{}'.format(fname), 'recreate')
    total_ss.Write()
    total_ss_norw.Write()
    total_data.Write()
    rf.Close()
   

def main():

    mass = ('M_MuMu_FSR', 450, (110, 200))

    higgs_score = ('ClassOut_XGB_fJVT_Higgs', 100, (0, 1))
    vbf_score = ('ClassOut_XGB_fJVT_VBF', 100, (0, 1))
    z_pt = ('DimuonPT', 100, (0, 400))
    z_rapidity = ('DimuonY', 100, (0, 6))
    costhetastar = ('AbsCosThetaStar', 100, (0, 1))

    leadjet_pt = ('LeadJetPt', 100, (0, 400))
    subjet_pt = ('SubleadJetPt', 100, (0, 400))
    dijet_pt = ('DijetPt', 100, (0, 400))

    leadjet_eta = ('LeadJetEta', 100, (0, 5))
    subjet_eta = ('SubleadJetEta', 100, (0, 5))
    dijet_rapidity = ('DijetRapidity', 100, (0, 6))

    dijet_mass = ('DijetM', 100, (0, 1000))
    met = ('EventMET', 100, (0, 200))

    # zero jet variables
    pairs = {}
    pairs[0] = [(mass, higgs_score),
                (mass, z_pt),
                (mass, z_rapidity),
                (mass, costhetastar),
                ]

    # one jet variables
    pairs[1] = [(mass, leadjet_pt),
                (mass, leadjet_eta),
                ]
    pairs[1].extend(pairs[0])

    # two jet variables
    pairs[2] = [(mass, subjet_pt),
                (mass, subjet_eta),
                (mass, dijet_pt),
                (mass, dijet_rapidity),
                (mass, dijet_mass),
                (mass, met),
                (mass, vbf_score),
                ]
    pairs[2].extend(pairs[1])

    # loop over all njets
    for njet in [0, 1, 2]:

        # loop over all variables
        for pair in pairs[njet]:
            print(njet, pair)
            print()
            mass, yvar = pair[0], pair[1]

            # main functions
            create_histograms(njet, mass, yvar)
            sum_histograms(njet, mass, yvar)


if __name__ == '__main__':
    main()
