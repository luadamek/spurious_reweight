import os
import ROOT as r
import numpy as np

sampling = 1
spurious_tree = "di_muon_ntuple"
data_tree = "DiMuonNtuple"

def get_ss_minituple_files(njets):
    loc = get_minituple_loc()
    files = [f for f in os.listdir(loc) if os.path.isfile(os.path.join(loc, f)) and "root" in f and "{}jet".format(njets) in f and "data" not in f]
    return files

def get_ss_ntuple_files(njets):
    loc = get_ss_ntuple_loc(njets)
    files = [f for f in os.listdir(loc) if os.path.isfile(os.path.join(loc, f)) and "root" in f and "data" not in f]
    return files

def get_data_ntuple_files(njets):
    loc = get_data_ntuple_loc(njets)
    files = [f for f in os.listdir(loc) if os.path.isfile(os.path.join(loc, f)) and "root" in f and "data" in f]
    return files

def get_data_minituple_files(njets):
    loc = get_minituple_loc()
    files = [f for f in os.listdir(loc) if os.path.isfile(os.path.join(loc, f)) and "root" in f and "{}jet".format(njets) in f and "data" in f]
    return files

def get_ntuple_loc(f):
    for njets in [0,1,2]:
        if f in get_data_ntuple_files(njets):
            return get_data_ntuple_loc(njets)
        if f in get_ss_ntuple_files(njets):
            return get_ss_ntuple_loc(njets)

def get_all_ntuples():
    all_files = []
    get_file_funcs = [get_data_ntuple_files, get_ss_ntuple_files]
    for njet in [0,1,2]:
        for func in get_file_funcs:
            files = func(njet)
            for f in files:
                if f not in all_files:
                    all_files.append(f)

    return all_files

def get_njets(f, file_list_retriever):
    njets = []
    for njet in [0, 1, 2]:
        if f in file_list_retriever(njet):
            njets.append(njet)
    return njets

def is_data_ntuple(f):
    for njet in [0,1,2]:
        if (f in get_data_ntuple_files(njet)):
            return True
    return False

def get_minituple_loc():
    """
    Location of ss_files for training.
    """

    minituple_locs = {
                 'mzgubic':'',
                 'zgubic':'/data/atlassmallfiles/users/zgubic/hmumu/spurious_signal/minituple/',
                 'ladamek':'/home/ladamek/projects/def-psavard/ladamek/minitrees_training',
                 'luadamek':'/eos/user/l/luadamek/HmumuSpuriousEvaluation/',
                 }
    user = os.getenv('USER')

    return minituple_locs[user]


def get_ss_ntuple_loc(njets):
    """
    Location of the prepared data, which is much smaller because of the invariant mass cut and because not many variables are needed.
    """

    ntuple_locs = {}

    ntuple_locs[0] = {
                 'mzgubic':'',
                 'zgubic':'/data/atlassmallfiles/users/zgubic/hmumu/spurious_signal/v12/mumu95_incl/',
                 'ladamek':'/home/ladamek/projects/def-psavard/ladamek/ss_v12/mumu95_incl/',
                 'luadamek':'/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/ss_ntuples/v12/mumu95_incl/',
                 }
    ntuple_locs[1] = {
                 'mzgubic':'',
                 'zgubic':'/data/atlassmallfiles/users/zgubic/hmumu/spurious_signal/v12/mumu95_incl/',
                 'ladamek':'/home/ladamek/projects/def-psavard/ladamek/ss_v12/mumu95_incl/',
                 'luadamek':'/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/ss_ntuples/v12/mumu95_incl/',
                 }
    ntuple_locs[2] = {
                 'mzgubic':'',
                 'zgubic':'/data/atlassmallfiles/users/zgubic/hmumu/spurious_signal/v12/mumu95_jj/',
                 'ladamek':'/home/ladamek/projects/def-psavard/ladamek/ss_v12/mumu95_jj/',
                 'luadamek':'/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/ss_ntuples/v12/mumu95_jj/',
                 }

    user = os.getenv('USER')

    return ntuple_locs[njets][user]

def get_data_ntuple_loc(njets):
    """
    Location of the prepared data, which is much smaller because of the invariant madata cut and because not many variables are needed.
    """

    ntuple_locs = {}

    ntuple_locs[0] = {
                 'mzgubic':'',
                 'zgubic':'/data/atlassmallfiles/users/zgubic/hmumu/v19/',
                 'ladamek':'/home/ladamek/projects/def-psavard/ladamek/v19_data/',
                 'luadamek':'/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/Hmumu/common_ntuples/v19/',
                 }
    ntuple_locs[1] = ntuple_locs[0]
    ntuple_locs[2] = ntuple_locs[0]

    user = os.getenv('USER')

    return ntuple_locs[njets][user]


def xgb_higgs_boundaries(njet, cat):

    assert cat in ['inclusive', 1, 2, 3], 'category not recognised'

    if cat == 'inclusive':
        return 0, 1

    if njet == 0:
        edges = [1, 0.75, 0.35, 0]
    if njet == 1:
        edges = [1, 0.78, 0.38, 0]
    if njet == 2:
        edges = [1, 0.48, 0.22, 0]

    upper = edges[:-1]
    lower = edges[1:]
    return lower[cat-1], upper[cat-1]

def xgb_vbf_boundaries(njet, cat):

    assert cat in ['inclusive', 1, 2, 3], 'category not recognised'

    if cat == 'inclusive':
        return -99999999, 99999999

    if njet in [0, 1]:
        return -10000, -9998

    elif njet == 2:
        edges = [1, 0.89, 0.77, 0.6]
        upper = edges[:-1]
        lower = edges[1:]
        return lower[cat-1], upper[cat-1]


def load_weight_histo(njet):

    # read the th1d
    rf = r.TFile('../histograms/ratio_{}jet.root'.format(njet), 'read')
    h = rf.Get('ratio_{}jet'.format(njet))
    h.SetDirectory(0)
    rf.Close

    # translate to numpy array of edges and contents
    bins = h.GetNbinsX()
    edges = np.linspace(0,1,bins+1)
    contents = np.zeros(bins)
    errors = np.zeros(bins)
    for b in range(bins):
        contents[b] = h.GetBinContent(b)
        errors[b] = h.GetBinError(b)

    return edges, contents, errors
