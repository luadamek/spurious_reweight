import uproot as ur
import numpy as np

def get_needed_variables(njet):
    return get_extra_variables() + get_training_variables(njet)

def get_extra_variables():
    return ['EventNumber', 'M_MuMu_FSR', 'Weight', 'ClassOut_XGB_fJVT_Higgs', 'ClassOut_XGB_fJVT_VBF']

def get_training_variables(njet):
    """
    Variables in the tree needed to construct the training variables.
    """
    variables = {0:['DimuonPT', 'DimuonY', 'AbsCosThetaStar']}
    variables[1] = variables[0] + ['LeadJetPt', 'LeadJetEta', 'LeadJetDPhiZ']
    variables[2] = variables[1] + ['SubleadJetPt', 'SubleadJetEta', 'SubleadJetDPhiZ', 'DijetPt', 'DijetM', 'DijetDPhiZ', 'DijetRapidity', 'EventMET']

    return variables[njet]


def _prepare_arrays(file_path, njet, fold, training):
    """
    Prepare the arrays: cuts and split into folds.
    """

    # determine whether it is a ss file
    tree_name = 'training_tree' 
    variables = get_needed_variables(njet)

    # open the file and extract the arrays
    f = ur.open(file_path)
    t = f[tree_name]
    a = t.arrays(variables)

    # training mass mask
    mass = a[b'M_MuMu_FSR']
    low = np.logical_and(mass > 110, mass < 120)
    high = np.logical_and(mass > 130, mass < 180)
    mass_mask = np.logical_or(low, high)

    # fold masks
    remainder = {'train':(fold+2)%3, 'valid':(fold+1)%3, 'test':fold}
    event_number = a[b'EventNumber']
    fold_masks = {fold:event_number % 3 == remainder[fold] for fold in remainder}

    out_a = {}
    for fold in fold_masks:

        # get the mask
        if training:
            mask = np.logical_and(mass_mask, fold_masks[fold])
        else:
            mask = fold_masks[fold]

        # copy each individual variable
        out_a[fold] = {}
        for v in variables:
            out_a[fold][v] = a[v.encode()][mask]

    return out_a


def get_X(file_path, njet, fold, training=True):
    """
    Get the appropriate X array from data
    """

    folds = _prepare_arrays(file_path, njet, fold, training=training)
    x_vars = get_training_variables(njet)

    out = {}
    for fold in folds:

        # prepare the array
        N_events = len(folds[fold]['DimuonPT'])
        N_vars = len(x_vars)
        out[fold] = np.zeros(shape=(N_events, N_vars))

        # fill the array
        for i, var in enumerate(x_vars):
            out[fold][:,i] = folds[fold][var]

    return out['train'], out['valid'], out['test']

def get_Z(file_path, njet, fold):
    """
    Get the ancillaries from data.
    """

    folds = _prepare_arrays(file_path, njet, fold, training=False)

    return folds['train'], folds['valid'], folds['test']




