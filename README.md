# spurious_reweight

Conduct study of using BDT score distribution to re-weight Spruious Signal MC to Data.

# Setup
```
git clone ssh://git@gitlab.cern.ch:7999/luadamek/spurious_reweight.git
source /cvmfs/sft-nightlies.cern.ch/lcg/views/dev3python3/latest/x86_64-centos7-gcc62-opt/setup.sh
python3 -m venv spurious_reweight
cd spurious_reweight
source bin/activate
source /cvmfs/sft-nightlies.cern.ch/lcg/views/dev3python3/latest/x86_64-centos7-gcc62-opt/setup.sh
mkdir PythonLocal
pip install --install-option="--prefix=$PWD/PythonLocal" uproot
source ./setup.sh
```

# Login
```
source ./setup.sh
```

# Download files
The following lines will download a subset of the SS samples for testing. To download all of them, set Test = False
```
cd scripts
voms-proxy-init --voms atlas
python copy_files.py --ss_flavour mumu95_incl --test True --outDir ~/projects/def-psavard/ladamek/ss_test_v12/mumu95_incl/
python copy_files.py --ss_flavour mumu95_jj --test True --outDir ~/projects/def-psavard/ladamek/ss_test_v12/mumu95_jj/
```

# Create training trees locally
Make sure that the directories in utils point to the correct place
```
cd macros
python prepare_training.py
```

# Flock jobs to condor
```
mkdir data
cd macros
python prepare_training.py --condor --user $USER --jobName production
```

# train the model
```
cd scripts
python train_models.py -n 0 f 0
```

