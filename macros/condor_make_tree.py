import argparse
from prepare_training import make_minitrees
parser=argparse.ArgumentParser(description='Produce minitrees for a file')
parser.add_argument('--file','-f',dest="file",type=str,default=str, required=True)
args = parser.parse_args()
make_minitrees(args.file)
print("The job finished!")
