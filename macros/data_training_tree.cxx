#include "TLorentzVector.h"

void data_training_tree(std::string infile, std::string ofile, std::string tree_name, Int_t njet_category) {

  //::: Get the old file, and fetch the old tree
  TFile* in_file = new TFile(infile.c_str());
  TTree* in_tree = (TTree*)in_file->Get( tree_name.c_str() );
  Long64_t nentries = in_tree->GetEntries();

  //::: Read all branches
  in_tree->SetBranchStatus("*", 1);

  //::: Prepare the new trees
  TFile *out_file = new TFile(ofile.c_str(), "RECREATE");
  TTree* out_tree = new TTree("training_tree", "training_tree");

  //Prepare all of the output training variables
  //The Zero jet Variables
  Float_t ClassOut_XGB_fJVT_Higgs;
  Float_t ClassOut_XGB_fJVT_VBF;
  ULong64_t EventNumber;
  Float_t Weight;
  Float_t M_MuMu_FSR;
  Float_t EventMET;
  Float_t DimuonPT;
  Float_t DimuonY;
  Float_t AbsCosThetaStar;

  //The One jet Variables
  Float_t LeadJetPt;
  Float_t LeadJetEta;
  Float_t LeadJetDPhiZ;
  Float_t LeadJetPhi;
  Float_t LeadJetE;

  //The Two jet Variables
  Float_t SubleadJetPt;
  Float_t SubleadJetEta;
  Float_t SubleadJetDPhiZ;
  Float_t SubleadJetPhi;
  Float_t SubleadJetE;

  Float_t DijetPt;
  Float_t DijetRapidity;
  Float_t DijetDPhiZ;
  Float_t DijetM;

  out_tree->Branch("EventNumber", &EventNumber);
  out_tree->Branch("Weight", &Weight);
  out_tree->Branch("M_MuMu_FSR", &M_MuMu_FSR);
  out_tree->Branch("EventMET", &EventMET);
  out_tree->Branch("DimuonPT", &DimuonPT);
  out_tree->Branch("DimuonY", &DimuonY);
  out_tree->Branch("AbsCosThetaStar", &AbsCosThetaStar);
  out_tree->Branch("ClassOut_XGB_fJVT_Higgs", &ClassOut_XGB_fJVT_Higgs);
  out_tree->Branch("ClassOut_XGB_fJVT_VBF", &ClassOut_XGB_fJVT_VBF);

  //The One jet Variables
  if (njet_category > 0){
    out_tree->Branch("LeadJetPt", &LeadJetPt);
    out_tree->Branch("LeadJetEta", &LeadJetEta);
    out_tree->Branch("LeadJetDPhiZ", &LeadJetDPhiZ);
    out_tree->Branch("LeadJetPhi", &LeadJetPhi);
    out_tree->Branch("LeadJetE", &LeadJetE);
  }

  //The Two jet Variables
  if (njet_category > 1){
    out_tree->Branch("SubleadJetPt", &SubleadJetPt);
    out_tree->Branch("SubleadJetEta", &SubleadJetEta);
    out_tree->Branch("SubleadJetDPhiZ", &SubleadJetDPhiZ);
    out_tree->Branch("SubleadJetPhi", &SubleadJetPhi);
    out_tree->Branch("SubleadJetE", &SubleadJetE);
    out_tree->Branch("DijetPt", &DijetPt);
    out_tree->Branch("DijetRapidity", &DijetRapidity);
    out_tree->Branch("DijetDPhiZ", &DijetDPhiZ);
    out_tree->Branch("DijetM", &DijetM);
  }

  // Everything needed in the zerojet case
  Float_t Muons_Minv_MuMu_Fsr;
  Float_t Event_MET;
  Float_t Z_PT_FSR;
  Float_t Z_Phi_FSR;
  Float_t Muons_CosThetaStar;
  ULong64_t EventInfo_EventNumber;
  Float_t GlobalWeight;;
  Float_t Z_Y_FSR;

  std::vector<Float_t> *Jets_PT =0;
  std::vector<Float_t> *Jets_Eta =0;
  std::vector<Float_t> *Jets_Phi =0;
  std::vector<Float_t> *Jets_E =0;
  std::vector<Float_t> *Jets_PassFJVT =0;

  //setup all of the variables needed
  in_tree->SetBranchStatus("*", 0);
  in_tree->SetBranchStatus("Muons_Minv_MuMu_Fsr", 1);
  in_tree->SetBranchAddress("Muons_Minv_MuMu_Fsr", &Muons_Minv_MuMu_Fsr);
  in_tree->SetBranchStatus("Event_MET", 1);
  in_tree->SetBranchAddress("Event_MET", &Event_MET);
  in_tree->SetBranchStatus("Z_PT_FSR", 1);
  in_tree->SetBranchAddress("Z_PT_FSR", &Z_PT_FSR);
  in_tree->SetBranchStatus("Z_Y_FSR", 1);
  in_tree->SetBranchAddress("Z_Y_FSR", &Z_Y_FSR);
  in_tree->SetBranchStatus("Z_Phi_FSR", 1);
  in_tree->SetBranchAddress("Z_Phi_FSR", &Z_Phi_FSR);
  in_tree->SetBranchStatus("Muons_CosThetaStar", 1);
  in_tree->SetBranchAddress("Muons_CosThetaStar", &Muons_CosThetaStar);
  in_tree->SetBranchStatus("EventInfo_EventNumber", 1);
  in_tree->SetBranchAddress("EventInfo_EventNumber", &EventInfo_EventNumber);
  in_tree->SetBranchStatus("Jets_PassFJVT", 1);
  in_tree->SetBranchAddress("Jets_PassFJVT", &Jets_PassFJVT);
  in_tree->SetBranchStatus("GlobalWeight", 1);
  in_tree->SetBranchAddress("GlobalWeight", &GlobalWeight);
  in_tree->SetBranchStatus("ClassOut_XGB_fJVT_Higgs", 1);
  in_tree->SetBranchAddress("ClassOut_XGB_fJVT_Higgs", &ClassOut_XGB_fJVT_Higgs);
  in_tree->SetBranchStatus("ClassOut_XGB_fJVT_VBF", 1);
  in_tree->SetBranchAddress("ClassOut_XGB_fJVT_VBF", &ClassOut_XGB_fJVT_VBF);
  

  if (njet_category > 0){
    in_tree->SetBranchStatus("Jets_PT", 1);
    in_tree->SetBranchAddress("Jets_PT", &Jets_PT);
    in_tree->SetBranchStatus("Jets_Eta", 1);
    in_tree->SetBranchAddress("Jets_Eta", &Jets_Eta);
    in_tree->SetBranchStatus("Jets_Phi", 1);
    in_tree->SetBranchAddress("Jets_Phi", &Jets_Phi);
    in_tree->SetBranchStatus("Jets_E", 1);
    in_tree->SetBranchAddress("Jets_E", &Jets_E);
  }
  
  Int_t n_jets;
  TLorentzVector j1;
  TLorentzVector j2;
  TLorentzVector dijet;
  Int_t first_jet = -1;
  Int_t second_jet = -1;

  //::: Loop over the tree, apply the cuts 
  for (Long64_t i=0; i<nentries; i++) {

    in_tree->GetEntry(i);
    //n_jets = Jets_PT->size();
    first_jet = -1;
    second_jet = -1;
    n_jets = 0;
    for (unsigned int i = 0; i < Jets_PassFJVT->size(); i++)
    {
        Int_t pass = Jets_PassFJVT->at(i);
        n_jets+=pass;
        if ( (pass > 0) && (first_jet < 0) ){first_jet = i;}
        else if ( (pass > 0) && (second_jet < 0) ){second_jet = i;}
    }

    bool fill = ( (n_jets == 0) and (njet_category == 0) ) || ( (n_jets == 1) and (njet_category == 1) ) || ( (n_jets > 1) and (njet_category > 1) );
    if (not fill){continue;}

    Weight = GlobalWeight;
    M_MuMu_FSR = Muons_Minv_MuMu_Fsr;
    EventMET = Event_MET;
    DimuonPT = Z_PT_FSR;
    DimuonY = Z_Y_FSR;
    AbsCosThetaStar = TMath::Abs(Muons_CosThetaStar);
    EventNumber = EventInfo_EventNumber;
    if (n_jets == 0){out_tree->Fill(); continue;}

    LeadJetPt = Jets_PT->at(first_jet);
    LeadJetEta = Jets_Eta->at(first_jet);
    LeadJetDPhiZ = TVector2::Phi_mpi_pi(Jets_Phi->at(first_jet) - Z_Phi_FSR);
    LeadJetPhi = Jets_Phi->at(first_jet);
    LeadJetE = Jets_E->at(first_jet);
     
    if (n_jets == 1){out_tree->Fill(); continue;}

    SubleadJetPt = Jets_PT->at(second_jet);
    SubleadJetEta = Jets_Eta->at(second_jet);
    SubleadJetDPhiZ = TVector2::Phi_mpi_pi(Jets_Phi->at(second_jet) - Z_Phi_FSR);
    SubleadJetPhi = Jets_Phi->at(second_jet);
    SubleadJetE = Jets_E->at(second_jet);

    j1.SetPtEtaPhiE(Jets_PT->at(first_jet), Jets_Eta->at(first_jet), Jets_Phi->at(first_jet), Jets_E->at(first_jet));
    j2.SetPtEtaPhiE(Jets_PT->at(second_jet), Jets_Eta->at(second_jet), Jets_Phi->at(second_jet), Jets_E->at(second_jet));
    dijet = j1 + j2;
    DijetPt = dijet.Pt();
    DijetRapidity = dijet.Rapidity();
    DijetDPhiZ = TVector2::Phi_mpi_pi(dijet.Phi() - Z_Phi_FSR);
    DijetM = dijet.M();
    if (n_jets > 1){out_tree->Fill();}

  }

  out_tree->AutoSave();
  delete in_file;
  delete out_file;

}

