void selection(std::string infile, std::string outfile, std::string tree_name, Int_t subsampling) {

  //::: Get the old file, and fetch the old tree
  TFile* in_file = new TFile(infile.c_str());
  TTree* in_tree = (TTree*)in_file->Get(tree_name.c_str());
  Long64_t nentries = in_tree->GetEntries();

  in_tree->SetBranchStatus("*", 1);
  // Skip these branches

  Float_t mc_weight = 0;
  Int_t hasbjet = 0;
  Int_t nmuons = 0;
  Int_t lead_charge = 0;
  Int_t sub_charge = 0;
  Int_t njets = 0;

  Float_t m_mumu_fsr;
  bool apply_analysis_sel = false;
  ULong64_t event_num = 0;
  if (tree_name == "di_muon_ntuple"){
      in_tree->SetBranchStatus("*Reso*", 0);
      in_tree->SetBranchStatus("m_mumu_sigma",0);
      in_tree->SetBranchAddress("m_mumu_fsr", &m_mumu_fsr);
  }
  if (tree_name == "DiMuonNtuple") {
      apply_analysis_sel = true;
      in_tree->SetBranchStatus("Truth*", 0);
      in_tree->SetBranchStatus("Muons_Pos*", 0);
      in_tree->SetBranchStatus("Muons_Neg*", 0);
      in_tree->SetBranchStatus("Electrons_Pos*", 0);
      in_tree->SetBranchStatus("Electrons_Neg*", 0);
      //in_tree->SetBranchStatus("FJVT*", 0);
      //in_tree->SetBranchStatus("*FJVT*", 0);
      in_tree->SetBranchStatus("*MCP*", 0);
      in_tree->SetBranchStatus("*CombFit*", 0);
      in_tree->SetBranchStatus("*SFWeight", 0);
      //in_tree->SetBranchStatus("*Pass*", 0);
      in_tree->SetBranchAddress("Muons_Minv_MuMu_Fsr", &m_mumu_fsr); 
      in_tree->SetBranchAddress("EventWeight_MCEventWeight", &mc_weight);
      in_tree->SetBranchAddress("Event_HasBJet", &hasbjet);
      in_tree->SetBranchAddress("Muons_Multip", &nmuons);
      in_tree->SetBranchAddress("Muons_Charge_Lead", &lead_charge);
      in_tree->SetBranchAddress("Muons_Charge_Sub", &sub_charge);
  }

  //::: Prepare the new tree
  TFile *out_file = new TFile(outfile.c_str(), "recreate");
  TTree *out_tree = in_tree->CloneTree(0);
  out_tree->SetName(tree_name.c_str());
  if (tree_name == "di_muon_ntuple") {out_tree->Branch("EventNumber", &event_num);}

  //::: Loop over the tree, apply the cuts 
  for (ULong64_t i=0; i<nentries; i++) {
    if ( (subsampling == 1) || (i % subsampling == 0) )
    {
        in_tree->GetEntry(i);
        if ( m_mumu_fsr > 100.0 ){
            if (apply_analysis_sel)
            {
                bool pass_sel = ( ( hasbjet == 0 ) && ( nmuons == 2 ) && ( abs(mc_weight) < 2000 ) && ( lead_charge * sub_charge < 0 ) );
                if (not pass_sel) continue;
            } // Analysis selections
            event_num = i;
            out_tree->Fill();
        } // mass > 110 GeV cut
    } // take one in every subsampling events
  } // loop over the tree

  //::: Clean up
  out_tree->AutoSave();
  delete in_file;
  delete out_file;

}

