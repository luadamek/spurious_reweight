#include "TLorentzVector.h"

void spurious_training_tree(std::string infile, std::string ofile, std::string tree_name, Int_t njet_category) {

  //::: Get the old file, and fetch the old tree
  TFile* in_file = new TFile(infile.c_str());
  TTree* in_tree = (TTree*)in_file->Get( tree_name.c_str() );
  Long64_t nentries = in_tree->GetEntries();

  //::: Copy all
  in_tree->SetBranchStatus("*", 1);

  //::: Prepare the new trees
  std::string open_type = "Write";
  TFile *out_file = new TFile(ofile.c_str(), "RECREATE");

  TTree* out_tree = new TTree("training_tree", "training_tree");

  //Prepare all of the output training variables
  //The Zero jet Variables
  Float_t ClassOut_XGB_fJVT_Higgs;
  Float_t ClassOut_XGB_fJVT_VBF;
  ULong64_t EventNumber;
  Float_t Weight;
  Float_t M_MuMu_FSR;
  Float_t EventMET;
  Float_t DimuonPT;
  Float_t DimuonY;
  Float_t AbsCosThetaStar;

  //The One jet Variables
  Float_t LeadJetPt;
  Float_t LeadJetEta;
  Float_t LeadJetDPhiZ;
  Float_t LeadJetPhi;
  Float_t LeadJetE;

  //The Two jet Variables
  Float_t SubleadJetPt;
  Float_t SubleadJetEta;
  Float_t SubleadJetDPhiZ;
  Float_t SubleadJetPhi;
  Float_t SubleadJetE;

  Float_t DijetPt;
  Float_t DijetRapidity;
  Float_t DijetDPhiZ;
  Float_t DijetM;

  out_tree->Branch("EventNumber", &EventNumber);
  out_tree->Branch("Weight", &Weight);
  out_tree->Branch("M_MuMu_FSR", &M_MuMu_FSR);
  out_tree->Branch("EventMET", &EventMET);
  out_tree->Branch("DimuonPT", &DimuonPT);
  out_tree->Branch("DimuonY", &DimuonY);
  out_tree->Branch("AbsCosThetaStar", &AbsCosThetaStar);
  out_tree->Branch("ClassOut_XGB_fJVT_Higgs", &ClassOut_XGB_fJVT_Higgs);
  out_tree->Branch("ClassOut_XGB_fJVT_VBF", &ClassOut_XGB_fJVT_VBF);

  //The One jet Variables
  if (njet_category > 0){
    out_tree->Branch("LeadJetPt", &LeadJetPt);
    out_tree->Branch("LeadJetEta", &LeadJetEta);
    out_tree->Branch("LeadJetDPhiZ", &LeadJetDPhiZ);
    out_tree->Branch("LeadJetPhi", &LeadJetPhi);
    out_tree->Branch("LeadJeTE", &LeadJetE);
  }

  //The Two jet Variables
  if (njet_category > 1){
    out_tree->Branch("SubleadJetPt", &SubleadJetPt);
    out_tree->Branch("SubleadJetEta", &SubleadJetEta);
    out_tree->Branch("SubleadJetDPhiZ", &SubleadJetDPhiZ);
    out_tree->Branch("SubleadJetPhi", &SubleadJetPhi);
    out_tree->Branch("SubleadJeTE", &SubleadJetE);
    out_tree->Branch("DijetPt", &DijetPt);
    out_tree->Branch("DijetRapidity", &DijetRapidity);
    out_tree->Branch("DijetDPhiZ", &DijetDPhiZ);
    out_tree->Branch("DijetM", &DijetM);
  }

  // Everything needed in the zerojet case
  Float_t McEventWeight;
  Float_t ExpWeight;
  Float_t Muons_PT_Lead;
  Float_t Muons_Eta_Lead;
  Float_t Muons_Phi_Lead;
  Float_t Muons_PT_Sub;
  Float_t Muons_Eta_Sub;
  Float_t Muons_Phi_Sub;
  Float_t m_mumu_fsr;
  Float_t MET;
  Float_t Fsr_PT;
  Float_t Fsr_Eta;
  Float_t Fsr_Phi;

  std::vector<Float_t> *Jets_PT =0;
  std::vector<Float_t> *Jets_Eta =0;
  std::vector<Float_t> *Jets_Phi =0;

  //setup all of the variables needed
  in_tree->SetBranchStatus("*", 0);
  in_tree->SetBranchStatus("Muons_PT_Lead", 1);
  in_tree->SetBranchAddress("Muons_PT_Lead", &Muons_PT_Lead);
  in_tree->SetBranchStatus("EventNumber", 1);
  in_tree->SetBranchAddress("EventNumber", &EventNumber);
  in_tree->SetBranchStatus("McEventWeight", 1);
  in_tree->SetBranchAddress("McEventWeight", &McEventWeight);
  in_tree->SetBranchStatus("ExpWeight", 1);
  in_tree->SetBranchAddress("ExpWeight", &ExpWeight);
  in_tree->SetBranchStatus("Muons_Eta_Lead", 1);
  in_tree->SetBranchAddress("Muons_Eta_Lead", &Muons_Eta_Lead);
  in_tree->SetBranchStatus("Muons_Phi_Lead", 1);
  in_tree->SetBranchAddress("Muons_Phi_Lead", &Muons_Phi_Lead);
  in_tree->SetBranchStatus("Muons_PT_Sub", 1);
  in_tree->SetBranchAddress("Muons_PT_Sub", &Muons_PT_Sub);
  in_tree->SetBranchStatus("Muons_Eta_Sub", 1);
  in_tree->SetBranchAddress("Muons_Eta_Sub", &Muons_Eta_Sub);
  in_tree->SetBranchStatus("Muons_Phi_Sub", 1);
  in_tree->SetBranchAddress("Muons_Phi_Sub", &Muons_Phi_Sub);
  in_tree->SetBranchStatus("Fsr_PT", 1);
  in_tree->SetBranchAddress("Fsr_PT", &Fsr_PT);
  in_tree->SetBranchStatus("Fsr_Eta", 1);
  in_tree->SetBranchAddress("Fsr_Eta", &Fsr_Eta);
  in_tree->SetBranchStatus("Fsr_Phi", 1);
  in_tree->SetBranchAddress("Fsr_Phi", &Fsr_Phi);
  in_tree->SetBranchStatus("m_mumu_fsr", 1);
  in_tree->SetBranchAddress("m_mumu_fsr", &m_mumu_fsr);
  in_tree->SetBranchStatus("MET", 1);
  in_tree->SetBranchAddress("MET", &MET);
  in_tree->SetBranchStatus("Jets_PT", 1);
  in_tree->SetBranchAddress("Jets_PT", &Jets_PT);
  in_tree->SetBranchStatus("ClassOut_XGB_fJVT_Higgs", 1);
  in_tree->SetBranchAddress("ClassOut_XGB_fJVT_Higgs", &ClassOut_XGB_fJVT_Higgs);
  in_tree->SetBranchStatus("ClassOut_XGB_fJVT_VBF", 1);
  in_tree->SetBranchAddress("ClassOut_XGB_fJVT_VBF", &ClassOut_XGB_fJVT_VBF);

  if (njet_category > 0){
    in_tree->SetBranchStatus("Jets_Eta", 1);
    in_tree->SetBranchAddress("Jets_Eta", &Jets_Eta);
    in_tree->SetBranchStatus("Jets_Phi", 1);
    in_tree->SetBranchAddress("Jets_Phi", &Jets_Phi);
  }
  
  Int_t n_jets;
  TLorentzVector mu1;
  TLorentzVector mu2;
  TLorentzVector mumu;
  TLorentzVector j1;
  TLorentzVector j2;
  TLorentzVector dijet;
  TLorentzVector fsr;
  TLorentzVector Z;
  
  Float_t Muon_Mass = 0.1056584; // GeV
  //::: Loop over the tree, apply the cuts 
  for (Long64_t i=0; i<nentries; i++) {

    in_tree->GetEntry(i);
    n_jets = Jets_PT->size();
    bool fill = ( (n_jets == 0) and (njet_category == 0) ) || ( (n_jets == 1) and (njet_category == 1) ) || ( (n_jets > 1) and (njet_category > 1) );
    if (not fill){continue;}

    M_MuMu_FSR = m_mumu_fsr;
    mu1.SetPtEtaPhiM(Muons_PT_Lead, Muons_Eta_Lead, Muons_Phi_Lead, Muon_Mass);
    mu2.SetPtEtaPhiM(Muons_PT_Sub, Muons_Eta_Sub, Muons_Phi_Sub, Muon_Mass);
    fsr.SetPtEtaPhiM(Fsr_PT, Fsr_Eta, Fsr_Phi, 0.0);
    Z = mu1 + mu2 + fsr;

    Float_t p1p = mu1.E() + mu1.Pz();
    Float_t p1m = mu1.E() - mu1.Pz();
    Float_t p2p = mu2.E() + mu2.Pz();
    Float_t p2m = mu2.E() - mu2.Pz();
    mumu = mu1+mu2;
    Float_t mumuM = mumu.M();

    Weight = McEventWeight * ExpWeight;
    EventMET = MET;
    AbsCosThetaStar = TMath::Abs((p1p*p2m-p1m*p2p)/(mumuM*TMath::Sqrt( (mumuM*mumuM)+(mumu.Pt()*mumu.Pt()) )));
    DimuonPT = Z.Pt();
    DimuonY = Z.Rapidity();
    if (n_jets == 0){out_tree->Fill(); continue;}

    LeadJetPt = Jets_PT->at(0);
    LeadJetEta = Jets_Eta->at(0);
    LeadJetPhi = Jets_Phi->at(0);
    LeadJetDPhiZ = TVector2::Phi_mpi_pi(Jets_Phi->at(0)-Z.Phi());
    LeadJetE = Jets_PT->at(0)*TMath::CosH(Jets_Eta->at(0));

    if (n_jets == 1){out_tree->Fill(); continue;}
    SubleadJetPt = Jets_PT->at(1);
    SubleadJetEta = Jets_Eta->at(1);
    SubleadJetPhi = Jets_Phi->at(1);
    SubleadJetDPhiZ = TVector2::Phi_mpi_pi(Jets_Phi->at(1)-Z.Phi());
    SubleadJetE = Jets_PT->at(1)*TMath::CosH(Jets_Eta->at(1));

    j1.SetPtEtaPhiE(LeadJetPt, LeadJetEta, LeadJetPhi, LeadJetE);
    j2.SetPtEtaPhiE(SubleadJetPt, SubleadJetEta, SubleadJetPhi, SubleadJetE);
    dijet = j1 + j2;
    DijetPt = dijet.Pt();
    DijetRapidity = dijet.Rapidity();
    DijetDPhiZ = TVector2::Phi_mpi_pi(dijet.Phi() - Z.Phi());
    DijetM = dijet.M();

    if (n_jets > 1){out_tree->Fill();}

  }

  out_tree->AutoSave();
  delete in_file;
  delete out_file;

}

