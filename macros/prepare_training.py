import os
import utils
import argparse
import subprocess as sp

def make_minitrees(fname):
    is_data = utils.is_data_ntuple(fname)
    if is_data:
        macro = os.path.join(os.getenv('SRC'), 'macros', 'data_training_tree.cxx')
        file_list_func = utils.get_data_ntuple_files
        tree_name = utils.data_tree

    else:
        macro = os.path.join(os.getenv('SRC'), 'macros', 'spurious_training_tree.cxx')
        file_list_func = utils.get_ss_ntuple_files
        tree_name = utils.spurious_tree
    njets = utils.get_njets(fname, file_list_func)

    print("File=" + fname)
    print("Processing njets=" + str(njets))
    in_file = os.path.join(utils.get_ntuple_loc(fname), fname)
    mid_file = os.path.join(utils.get_minituple_loc(), fname + '.intermediate.root')

    # run the selection macro
    sel_macro = os.path.join(os.getenv('SRC'), 'macros', 'selection.cxx')
    command = "root -l -q '{m}(\"{i}\", \"{o}\", \"{t}\", {s})'".format(m=sel_macro, i=in_file, o=mid_file, t=tree_name, s=utils.sampling)
    os.system(command)

    #create the minitrees for each jet multiplicity
    for njet in njets:
        out_file = os.path.join(utils.get_minituple_loc(), fname[:-5] + "_{}jet.root".format(njet))
        command = "root -l -q '{m}(\"{i}\", \"{o}\", \"{t}\", {n})'".format(m=macro, i=mid_file, o=out_file, t=tree_name, n = njet)
        os.system(command)
        
    os.system('rm {}'.format(mid_file))

def main():
    # loop over all of the files
    for fname in utils.get_all_ntuples():
        make_minitrees(fname)

def flock_to_condor(job_name, user):
    job_counter = 0
    job_names = []
    for fname in utils.get_all_ntuples():
        make_condor_job(fname, job_name + "_{}".format(job_counter), user)
        job_names.append(job_name + "_{}".format(job_counter))
        job_counter += 1

def make_condor_job(fname, job_name, user):
    commands = []
    commands.append("cd macros")
    commands.append('export USER="{}"'.format(user))
    commands.append("python condor_make_tree.py --file {}".format(fname))
    job_dir =  os.path.join("{}/data/run".format(os.getenv('SRC')), job_name)
    write_script(commands, job_dir ,job_name)
    submit_condor(job_dir, job_name)

def submit_condor(job_dir, job_name):

    os.chdir(job_dir)
    condor_submit = os.path.join(job_dir, job_name + ".submit")

    with open(condor_submit, 'w') as f:
        f.write('Universe = vanilla\n')
        f.write('executable     = {}\n'.format(os.path.join(job_dir, job_name + ".sh")))
        f.write('transfer_input_files = {s}/macros, {s}/spurious_reweight, {s}/setup.sh \n'.format(s=os.getenv('SRC')))
        f.write('output         = {}/$(ClusterId).out\n'.format(job_dir))
        f.write('error          = {}/$(ClusterId).err\n'.format(job_dir))
        f.write('log            = {}/$(ClusterId).log\n'.format(job_dir))
        f.write('stream_output  = True\n')
        f.write('stream_error   = True\n')

        f.write('+JobFlavour     = "longlunch"\n')
        f.write('queue\n')

    # call the job submitter
    os.system('condor_submit ' + condor_submit)
    print("submitted '" + condor_submit + "'")
    os.chdir(os.getenv('SRC'))


 

def write_script(commands, job_path, job_name):

    # compile the job template
    job_contents = [
        '#!/bin/sh',
        'export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase',
        'alias setupATLAS=\'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\'',
        'setupATLAS',
        'pwd',
        'env',
        'source {}/setup.sh'.format(os.getenv('SRC')),
    ]
    for command in commands:
        print(command)
        job_contents.append(command)

    # make the run dir if it doesn't exist
    job_dir = os.path.join(os.getenv('SRC'), 'data/run/')
    if not os.path.isdir(job_dir):
        os.makedirs(job_dir)

    # write job contents
    if not os.path.isdir(job_path):
        os.makedirs(job_path)
    f = open( os.path.join(job_path, job_name +".sh"), 'w' )
    for line in job_contents:
        f.write( line + '\n' )
    f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Produce mini trees')
    parser.add_argument('--user', '-user', dest="user", type=str, required=False, default="", help='Username')
    parser.add_argument('--condor', '-c', dest="condor",default=False, action='store_true')
    parser.add_argument('--jobName', '-jn', dest="jobName", type=str, default="") 
    args = parser.parse_args()

    if args.condor and args.user=="":
        raise ValueError("Need username! Pass it with --user flag")
    if args.condor and args.jobName=="":
        raise ValueError("Need jobName! Pass it with --jobName flag")
    if args.condor:
        flock_to_condor(args.jobName, args.user)
    else:
        main()
