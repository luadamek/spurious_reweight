import argparse, pickle, sys, math, os
import xgboost as xgb
import numpy as np
import ROOT as r
sys.path.insert(0, '../..')
from utils import utils

def runEvents(in_tree, nStart, nEnd, out_file, models, transformers):

    catlabel = ['zero_jet', 'one_jet', 'two_jet']

    # python arrays to buffer MVA info
    jetsX = [[[] for _ in range(4)] for _ in range(3)]
    dijetX = [[[] for _ in range(4)] for _ in range(3)]
    metX = [[[] for _ in range(4)] for _ in range(3)]
    dimuonX = [[[] for _ in range(4)] for _ in range(3)]
    extraX = [[[] for _ in range(4)] for _ in range(3)]
    tscore = [[[] for _ in range(4)] for _ in range(3)]
    tscoreVBF = [[] for _ in range(4)]

    cat_index = []

    #####################
    ### first loop to fill Python arrays with MVA inputs, sorted by njet and fold
    #####################

    Muons_Lead = r.TLorentzVector()
    Muons_Sub = r.TLorentzVector()
    Fsr= r.TLorentzVector()
    j1, j2 = r.TLorentzVector(), r.TLorentzVector()

    for index, entry in enumerate(in_tree):
        if index < nStart: continue
        if index >= nEnd: break

        # di-muon + FSR info
        Muons_Lead.SetPtEtaPhiM(entry.Muons_PT_Lead, entry.Muons_Eta_Lead, entry.Muons_Phi_Lead, 0.106)
        Muons_Sub.SetPtEtaPhiM(entry.Muons_PT_Sub, entry.Muons_Eta_Sub, entry.Muons_Phi_Sub, 0.106)
        Fsr.SetPtEtaPhiM(entry.Fsr_PT, entry.Fsr_Eta, entry.Fsr_Phi, 0)

        mumu = Muons_Lead+Muons_Sub
        mumu_Fsr = mumu+Fsr
        mumuM = mumu.M()
        mumu_sigma = 0.5 * mumuM * math.sqrt(entry.Muons_ResoPT_Lead**2+entry.Muons_ResoPT_Sub**2)
        mumu_Fsr_Phi = mumu_Fsr.Phi()

        #if entry.Muons_Charge_Lead < 0: # forgot to write this variable in SS ntuple <=v11!
        p1p = Muons_Lead.E() + Muons_Lead.Pz()
        p1m = Muons_Lead.E() - Muons_Lead.Pz()
        p2p = Muons_Sub.E() + Muons_Sub.Pz()
        p2m = Muons_Sub.E() - Muons_Sub.Pz()
        # else:
        #     p2p = Muons_Lead.E() + Muons_Lead.Pz()
        #     p2m = Muons_Lead.E() - Muons_Lead.Pz()
        #     p1p = Muons_Sub.E() + Muons_Sub.Pz()
        #     p1m = Muons_Sub.E() - Muons_Sub.Pz()
        CosThetaStar = (p1p*p2m-p1m*p2p)/(mumuM*math.sqrt(mumuM**2+mumu.Pt()**2))
        if mumu.Pz() < 0: CosThetaStar *= -1.

        #####################
        ### Single Jet Info 
        #####################

        jet_info = []
            
        for jet_index in range(len(entry.Jets_PT)):
            Jets_E = entry.Jets_PT[jet_index]*math.cosh(entry.Jets_Eta[jet_index])
            jet_info.append([entry.Jets_PT[jet_index], entry.Jets_Eta[jet_index], r.TVector2.Phi_mpi_pi(entry.Jets_Phi[jet_index]-mumu_Fsr_Phi), entry.Jets_Phi[jet_index], Jets_E])

        njet = len(jet_info)
        cat = min(njet, 2)

        cat_index.append(cat)
        extraX[cat][index%4].append([mumuM, mumu_Fsr.M(), mumu_sigma])

        jet_info = sorted(jet_info, key=lambda x: x[0], reverse=True)
        if njet > 2:
            for jet_index in range(len(jet_info) - 2):
                jet_info.pop()
        elif njet < 2:
            for jet_index in range(2 - len(jet_info)):
                jet_info.append([-9999.] * 5)

        jetsX[cat][index%4].append(jet_info)

        #####################
        ### Di-Jet Info 
        #####################

        dijet_info = []
        if cat == 2:
            j1.SetPtEtaPhiE(jet_info[0][0], jet_info[0][1], jet_info[0][3], jet_info[0][4])
            j2.SetPtEtaPhiE(jet_info[1][0], jet_info[1][1], jet_info[1][3], jet_info[1][4])
            dijet = j1 + j2
            dijet_info.append([dijet.Pt(), dijet.Rapidity(), r.TVector2.Phi_mpi_pi(dijet.Phi()-mumu_Fsr_Phi), dijet.M()])
        else:
            dijet_info.append([-9999] * 4)

        dijetX[cat][index%4].append(dijet_info)

        #####################
        ### MET Info 
        #####################
        
        met_info = None
        if cat == 2:
            met_info = np.array([[entry.MET, -9999]])
        else:
            met_info = np.array([[-9999, -9999]])

        metX[cat][index%4].append(met_info)

        #####################
        ### Di-Muon Info 
        #####################

        dimuon_info = np.array([[mumu_Fsr.Pt(), mumu_Fsr.Rapidity(), -9999, abs(CosThetaStar)]])
        dimuonX[cat][index%4].append(dimuon_info)

        #####################
        ### Printing 
        #####################

        if index % 10000 == 0:
            print '{} -- Executing event: {}'.format(utils.debug_tag, index)
            print
            
    #####################
    ### Evaluation
    #####################

    alleval = [ [icat,imod] for icat in range(3) for imod in range (4)]
    for icat, imod in alleval:
        jetsXX = np.array(jetsX[icat][imod])
        dijetXX = np.array(dijetX[icat][imod])
        dimuonXX = np.array(dimuonX[icat][imod])
        metXX = np.array(metX[icat][imod])
    
        eval_array = []
        eval_array.append(jetsXX.reshape((jetsXX.shape[0], -1)))
        eval_array.append(metXX.reshape((metXX.shape[0], -1)))
        eval_array.append(dijetXX.reshape((dijetXX.shape[0], -1)))
        eval_array.append(dimuonXX.reshape((dimuonXX.shape[0], -1)))
        eval_array = np.concatenate(eval_array, axis=1)
        eval_array = xgb.DMatrix(eval_array)
    
        score = models[icat][imod].predict(eval_array)
        trans_score = transformers[icat][imod].transform(score.reshape(-1,1)).reshape(-1)
        tscore[icat][imod] = trans_score

        if icat == 2:
            score = models['VBF'][imod].predict(eval_array)
            trans_score = transformers['VBF'][imod].transform(score.reshape(-1,1)).reshape(-1)
            tscoreVBF[imod] = trans_score


    #####################
    ### Storage loop
    #####################

    out_file = r.TFile(out_file, 'recreate')
    out_tree = in_tree.CloneTree(0)

    add_ons, add_branches = {}, {}
    utils.add_variable(add_ons, add_branches, out_tree, 'ClassOut_XGB_fJVT_Higgs', 'F')
    utils.add_variable(add_ons, add_branches, out_tree, 'ClassOut_XGB_fJVT_VBF', 'F')
    utils.add_variable(add_ons, add_branches, out_tree, 'm_mumu', 'F')
    utils.add_variable(add_ons, add_branches, out_tree, 'm_mumu_fsr', 'F')
    utils.add_variable(add_ons, add_branches, out_tree, 'm_mumu_sigma', 'F')

    # index counters for each category
    idxcat = [[0 for _ in range(4)] for _ in range(3)]

    for index, entry in enumerate(in_tree):
        if index < nStart: continue
        if index >= nEnd: break

        mod = (index % 4)
        cat = cat_index[index-nStart]

        add_ons['ClassOut_XGB_fJVT_Higgs'][0] = 1. - tscore[cat][mod][idxcat[cat][mod]]
        if cat == 2:
            add_ons['ClassOut_XGB_fJVT_VBF'][0] = 1. - tscoreVBF[mod][idxcat[cat][mod]]
        else:
            add_ons['ClassOut_XGB_fJVT_VBF'][0] = -9999. 

        add_ons['m_mumu'][0] = extraX[cat][mod][idxcat[cat][mod]][0]
        add_ons['m_mumu_fsr'][0] = extraX[cat][mod][idxcat[cat][mod]][1]
        add_ons['m_mumu_sigma'][0] = extraX[cat][mod][idxcat[cat][mod]][2]

        idxcat[cat][mod] += 1

        #####################
        ### Printing 
        #####################

        if index % 10000 == 0:
            print '{} -- Executing event: {}'.format(utils.debug_tag, index)
            print '        en: ', index
            print '  fjvt_cat: ', catlabel[cat]
            print 'fjvt_higgs: ', add_ons['ClassOut_XGB_fJVT_Higgs'][0]
            print '  fjvt_vbf: ', add_ons['ClassOut_XGB_fJVT_VBF'][0]
            print
            
        out_tree.Fill()

        #####################
        ### Evaluation 
        #####################

    out_tree.AutoSave()
    out_file.Close()
         
    print '\n{} -- Finished!\n'.format(utils.info_tag, in_tree.GetEntries())

def get_model_from(input_file):
    model = xgb.Booster()
    model.load_model(input_file)
    return model

def get_transformer_from(input_file):
    transformer = pickle.load(open(input_file, 'rb'))
    return transformer

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='input file', action='store')
    parser.add_argument('-o', '--output', help='output file', action='store')
    parser.add_argument('-t', '--tree', help='tree_name', action='store', default='di_muon_ntuple')
    args = parser.parse_args()

    print
    for k, v in args._get_kwargs():
        print '  {:20}:  {}'.format(k, v)
    print

    models = {}
    transformers = {}

    catlabel = ['zero_jet', 'one_jet', 'two_jet']
    for cat in range(len(catlabel)):
        models[cat] = { model_index: get_model_from('models/conf/fjvt/{}_{}.h5'.format(catlabel[cat], model_index)) for model_index in range(4)}
        transformers[cat] = { model_index: get_transformer_from('models/conf/fjvt/score_transformer_{}_{}.pkl'.format(catlabel[cat], model_index)) for model_index in range(4)}
        if cat == 2:
            models['VBF'] = { model_index: get_model_from('models/conf/fjvt/{}_{}.h5'.format('VBF_two_jet', model_index)) for model_index in range(4)}
            transformers['VBF'] = { model_index: get_transformer_from('models/conf/fjvt/score_transformer_{}_{}.pkl'.format('two_jet_VBF', model_index)) for model_index in range(4)}

    print '{} -- Printing available (standard) models (dictionary):'.format(utils.debug_tag)
    for cat in models:
        print ' ', cat
        for index, model in models[cat].iteritems():
            print '   ', index, ': ', str(model)
        for index, transformer in transformers[cat].iteritems():
            print '   ', index, ': ', str(transformer)
    print

    print '{} -- Looking at {} tree'.format(utils.debug_tag, args.tree)
    in_file = r.TFile.Open(args.input)
    in_tree = in_file.Get(args.tree)

    neventTotal = in_tree.GetEntries()
    batchSize = 1000000
    print 'Tree has %i entries, processing in batches of %i events' % (neventTotal, batchSize)

    filelist = []
    for ibatch in range(max(neventTotal/batchSize,1)):
        nStart = ibatch*batchSize
        nEnd = nStart+batchSize
        if nEnd+batchSize > neventTotal: nEnd = neventTotal

        tmpfile = args.output.replace(".root", "_%i.root" % ibatch)
        filelist.append(tmpfile)
        print 'Run events %i to %i to file %s' % (nStart, nEnd, tmpfile)
        runEvents(in_tree, nStart, nEnd, tmpfile, models, transformers)

    # now mwerge files and copy lumi hist
    os.system("hadd %s %s" % (args.output, ' '.join(filelist)))

    outfile = r.TFile(args.output, "Update")
    h = in_file.Get("lumi")
    if h == None or not h.InheritsFrom("TH1"):
        print "No lumi saved?"
    else:
        h.Write()
    outfile.Close()
    in_file.Close()
    os.system("rm -f %s" % (' '.join(filelist)))

if __name__ == '__main__':
    sys.exit(main())
