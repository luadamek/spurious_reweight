export SRC=$PWD
source /cvmfs/sft-nightlies.cern.ch/lcg/views/dev3python3/latest/x86_64-centos7-gcc62-opt/setup.sh
export PYTHONPATH=${SRC}/PythonLocal/lib/python3.6/site-packages:$PYTHONPATH
export PYTHONPATH=${SRC}/spurious_reweight:$PYTHONPATH
echo "READY TO GO!"
